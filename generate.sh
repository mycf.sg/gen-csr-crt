#!/bin/bash
CONFIG_FILE="./config"
KEY_BITS=4096
GENERATE_CRT=0
usage() {
  echo "This generates a .csr file with private keys"
  echo "usage: csr.sh [-f config] -v"
  echo "-f,--file     path to config file, default=./config"
  echo "-v,--verbose  set bash to eux mode, output individual script commands for debugging"
}
while [ "$1" != "" ]; do
  case $1 in
  -f | --file)
    shift
    CONFIG_FILE=$1
    ;;
  -v | --verbose)
    VERBOSE=1
    ;;
  -h | --help)
    usage
    exit
    ;;
  -b | --bit)
    shift
    KEY_BITS=$1
    ;;
  -crt)
    GENERATE_CRT=1
    ;;
  *)
    usage
    exit 1
    ;;
  esac
  shift
done

if [ -n "$VERBOSE" ]; then
  set -eux
else
  set -eu
fi

retrieve() {
  local val=$(grep --color=never -Po "^${1}=\K.*" "${CONFIG_FILE}")
  if [ ! -n "${val}" ]; then
    echo "configuration file missing value for ${1}"
    exit 1
  fi
  echo -n "${val}"
}

KEY_NAME=$(retrieve "key_name")
KEY_PASSWORD=$(retrieve "key_password")
COMMON_NAME=$(retrieve "common_name")
ORGANISATION_NAME=$(retrieve "organization_name")
ORGANISATION_UNIT=$(retrieve "organization_unit")
EMAIL_ADDRESS=$(retrieve "email_address")
STATE_NAME=$(retrieve "state_name")
LOCALITY=$(retrieve "locality")
FOLDER_PATH="./certs/${KEY_NAME}"

printf -- "Making directory for certificates\n"
mkdir -p ./certs/"${KEY_NAME}"

printf -- "Generating Private Key in %s\n" "${FOLDER_PATH}/${KEY_NAME}"
openssl genrsa -aes-256-cbc -out "${FOLDER_PATH}/${KEY_NAME}.key" -passout pass:"${KEY_PASSWORD}" "${KEY_BITS}"

printf -- "Generating Public Key in %s\n" "${FOLDER_PATH}/${KEY_NAME}.public.key"
openssl rsa -in "${FOLDER_PATH}/${KEY_NAME}.key" -pubout -out "${FOLDER_PATH}/${KEY_NAME}.public.key" -passin pass:"${KEY_PASSWORD}"

printf -- "Generating CSR %s\n" "${FOLDER_PATH}/${KEY_NAME}.csr"
openssl req -new \
  -key "${FOLDER_PATH}/${KEY_NAME}.key" \
  -passin pass:"${KEY_PASSWORD}" \
  -out "${FOLDER_PATH}/${KEY_NAME}.csr" \
  -subj "/C=SG/ST=${STATE_NAME}/L=${LOCALITY}/O=${ORGANISATION_NAME}/OU=${ORGANISATION_UNIT}/CN=${COMMON_NAME}/emailAddress=${EMAIL_ADDRESS}"

if [ ${GENERATE_CRT} -eq 1 ]; then
  printf -- "Generating CRT %s\n" "${FOLDER_PATH}/${KEY_NAME}.crt"
  openssl x509 -req -days 365 -in "${FOLDER_PATH}/${KEY_NAME}.csr" \
    -signkey "${FOLDER_PATH}/${KEY_NAME}.key" -sha256 -passin pass:"${KEY_PASSWORD}" \
    -out "${FOLDER_PATH}/${KEY_NAME}.crt"

  printf -- "Generating PEM %s\n" "${FOLDER_PATH}/${KEY_NAME}.pem"
  openssl x509 -in "${FOLDER_PATH}/${KEY_NAME}.crt"  -out "${FOLDER_PATH}/${KEY_NAME}.pem" -outform PEM
fi
