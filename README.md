# gen-csr-crt

Generate CSR and self signed CRT for use with Application Gateways that require certificate authentication. Certificates are self signed as we only need to prove authenticity through the web portal of the API Gateway we're using
